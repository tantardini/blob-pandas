# Changelog

<!--next-version-placeholder-->

## v0.1.4 (2023-04-18)
### Fix
* Using underscore instead of hypens for package name ([`407686a`](https://gitlab.com/tantardini/blob-pandas/-/commit/407686afcc62c0451bc7703a5244e8602799a302))

## v0.1.3 (2023-04-18)
### Fix
* Fixed typo ([`ce5630b`](https://gitlab.com/tantardini/blob-pandas/-/commit/ce5630bd07ec619d5f2af84f6dbbfc4023ced84c))
* Changed package name ([`f014c2e`](https://gitlab.com/tantardini/blob-pandas/-/commit/f014c2ee2d6f710faba13fdec29ed517c720072d))

## v0.1.2 (2023-04-18)
### Fix
* Added explicit specification of package for build ([`88294a1`](https://gitlab.com/tantardini/blob-pandas/-/commit/88294a19ba1f82bc26709a13c901fd2da4e35bdd))

## v0.1.1 (2023-04-18)
### Fix
* Added build backend system for poetry ([`9c184c4`](https://gitlab.com/tantardini/blob-pandas/-/commit/9c184c4510c24373434485d3e1c56c6f9fd1a816))

## v0.1.0 (2023-04-18)
### Feature
* Added class to communicate with blob ([`1886101`](https://gitlab.com/tantardini/blob-pandas/-/commit/1886101fcb8d3c579711404655ea216b3aaf1947))
