# blob-pandas
Classes and utils to communicate with Azure Blob Storage through pandas DataFrames.

## Installation
```
pip install blob-pandas
```

## Usage
Describe how to launch and use blob-pandas project.
